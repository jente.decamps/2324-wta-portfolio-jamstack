import Post from "@/interfaces/Post";
import styles from "./page.module.css";
import fetchGraphQL from "@/services/GraphQLService";
import generateRssFeed from "@/helpers/GenerateRssFeed";

export const dynamic = "force-static";
export default async function Home() {
  const query = `query {
    posts {
      id
      title
      message
      createdAt
    }
  }`;
  const posts: Post[] = await fetchGraphQL(query).then((data) => data.posts);
  generateRssFeed(posts);
  return (
    <main className={styles.main}>
      <h1>Welcome to my portfolio!</h1>
      <p>
        This is a work in progress. Check back later for more content! You can
        already see some of my posts on the <a href="/posts">posts page.</a>
      </p>
    </main>
  );
}
