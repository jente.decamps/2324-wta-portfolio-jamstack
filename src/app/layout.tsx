import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import PortfolioHeader from "@/components/organisms/PortfolioHeader.tsx/PortfolioHeader";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Portfolio",
  description: "My portfolio website",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <PortfolioHeader />
        {children}
        <footer>Portfolio - Made by Jente & Senna - 2024</footer>
      </body>
    </html>
  );
}
