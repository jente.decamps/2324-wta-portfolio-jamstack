"use client";

import ClipboardCopyButton from "@/components/atoms/ClipboardCopyButton/ClipboardCopyButton";
import styles from "./page.module.css";
import PostCard from "@/components/molecules/PostCard/PostCard";
import Post from "@/interfaces/Post";
import fetchGraphQL from "@/services/GraphQLService";
import { useEffect, useState } from "react";

export const dynamic = "force-static";
export default function Posts() {
  const [search, setSearch] = useState<string>("");
  const [posts, setPosts] = useState<Post[]>([]);

  useEffect(() => {
    const query = `query {
      posts (term: "${search}") {
        id
        title
        message
        createdAt
      }
    }`;

    fetchGraphQL(query).then((data) =>
      setPosts(
        data.posts.sort(
          (a: Post, b: Post) =>
            new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
        )
      )
    );
  }, [search]);

  return (
    <main className={styles.main}>
      <div className={styles.titleContainer}>
        <h1>Posts</h1>
        <ClipboardCopyButton
          label="Copy RSS-feed link"
          textToCopy={process.env.NEXT_PUBLIC_FEED_URL || ""}
        />
        <input
          className={styles.search}
          type="text"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          placeholder="Search posts..."
        />
      </div>
      <ul className={styles.posts}>
        {posts.map((post) => (
          <li className={styles.post} key={post.id}>
            <PostCard post={post} />
          </li>
        ))}
      </ul>
    </main>
  );
}
