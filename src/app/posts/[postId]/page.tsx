import Post from "@/interfaces/Post";
import DateFormat from "@/helpers/DateFormat";
import styles from "./page.module.css";
import CommentCard from "@/components/molecules/CommentCard/CommentCard";
import Comment from "@/interfaces/Comment";
import { notFound } from "next/navigation";
import fetchGraphQL from "@/services/GraphQLService";

export const dynamic = "force-dynamic";
export default async function PostDetailPage({
  params,
}: {
  params: { postId: number };
}) {
  if (!Number(params.postId)) {
    return notFound();
  }

  const query = `query {
    postById(id: ${params.postId}){
      id
      title
      message
      createdAt
      comments {
        author
        message
        createdAt
      }
    }
  }`;

  const post: Post = await fetchGraphQL(query).then((data) => data.postById);

  if (!post) {
    return notFound();
  }

  post.comments?.sort((a: Comment, b: Comment) => {
    return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime();
  });

  return (
    <main className={styles.main}>
      <div className={styles.post}>
        <h1>{post.title}</h1>
        <p>{post.message}</p>
        <p className={styles.createdAt}>
          {DateFormat(new Date(post.createdAt))}
        </p>
      </div>
      <div className={styles.commentsContainer}>
        <div className={styles.commentsHeader}>
          <h2>Comments</h2>
          <a href={`/posts/${post.id}/comments/create`}>+ Add comment</a>
        </div>
        <ul className={styles.comments}>
          {post.comments ? (
            post.comments.map((comment, index) => (
              <li className={styles.comment} key={index}>
                <CommentCard comment={comment} />
                {index !== post.comments.length - 1 && (
                  <hr className={styles.hr} />
                )}
              </li>
            ))
          ) : (
            <li>
              <p>No comments yet for this post</p>
            </li>
          )}
        </ul>
      </div>
    </main>
  );
}
