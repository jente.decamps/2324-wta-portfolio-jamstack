"use client";

import styles from "./page.module.css";
import { FormEvent, useState } from "react";
import { notFound, useRouter } from "next/navigation";
import fetchGraphQL from "@/services/GraphQLService";

export const dynamic = "force-static";
export default function CreateCommentPage({
  params,
}: {
  params: { postId: number };
}) {
  const router = useRouter();

  const [author, setAuthor] = useState<string>("");
  const [comment, setComment] = useState<string>("");
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);

  if (!Number(params.postId)) {
    return notFound();
  }

  const onSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setIsLoading(true);
    setError(null);

    if (!author.trim() || !comment.trim()) {
      setError("Please fill in all fields");
      setIsLoading(false);
      return;
    }

    const query = `mutation {
      addComment(
        postId: "${params.postId}",
        author: "${author}",
        message: "${comment}",
      ) {id}
    }`;

    await fetchGraphQL(query);

    setIsLoading(false);
    router.back();
  };

  return (
    <main className={styles.main}>
      <h1>Write your comment</h1>
      {error && <p className={styles.error}>{error}</p>}
      <form onSubmit={onSubmit} className={styles.form}>
        <div className={styles.formField}>
          <label htmlFor="author">Author</label>
          <input
            className={styles.input}
            type="text"
            name="author"
            id="author"
            value={author}
            onChange={(e) => setAuthor(e.target.value)}
          />
        </div>

        <div className={styles.formField}>
          <label htmlFor="comment">Comment</label>
          <textarea
            className={styles.textarea}
            name="comment"
            id="comment"
            value={comment}
            onChange={(e) => setComment(e.target.value)}
          />
        </div>

        <button className={styles.button} type="submit" disabled={isLoading}>
          {isLoading ? "Loading..." : "Add your comment"}
        </button>
      </form>
    </main>
  );
}
