export default function Custom404() {
  return (
    <h1 style={{ margin: 16, textAlign: "center", flexGrow: 1 }}>404 - Page Not Found</h1>
  );
}
