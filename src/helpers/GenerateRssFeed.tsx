import RSS from "rss";
import fs from "fs";
import Post from "@/interfaces/Post";

export default function generateRssFeed(posts: Post[]) {
  const feed = new RSS({
    title: `Blogposts from ${process.env.NEXT_PUBLIC_USERNAME} his portfolio`,
    description: `This rss feed contains all the blogposts from ${process.env.NEXT_PUBLIC_USERNAME} his portfolio`,
    feed_url: process.env.NEXT_PUBLIC_FEED_URL || "",
    site_url: process.env.NEXT_PUBLIC_SITE_URL || "",
    image_url: process.env.NEXT_PUBLIC_LOGO_URL,
    pubDate: new Date(),
    copyright: `All rights reserved ${new Date().getFullYear()}, ${
      process.env.NEXT_PUBLIC_USERNAME
    }`,
  });

  posts.forEach((post) => {
    feed.item({
      title: post.title,
      description: post.message,
      url: `${process.env.NEXT_PUBLIC_SITE_URL}/posts/${post.id}`,
      date: post.createdAt,
    });
  });

  fs.writeFileSync("./public/rss.xml", feed.xml({ indent: true }));
}
