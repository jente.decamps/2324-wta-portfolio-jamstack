"use client";

import copy from "clipboard-copy";
import { useState } from "react";
import styles from "./ClipboardCopyButton.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faRss } from "@fortawesome/free-solid-svg-icons";

export default function ClipboardCopyButton({
  textToCopy,
  label,
}: {
  textToCopy: string;
  label: string;
}) {
  const [isCopied, setIsCopied] = useState(false);

  const handleCopyClick = async () => {
    try {
      await copy(textToCopy);
      setIsCopied(true);
      setTimeout(() => {
        setIsCopied(false);
      }, 2000);
    } catch (err) {
      console.error(err);
    }
  };
  return (
    <div className={styles.container}>
      {isCopied ? (
        <FontAwesomeIcon icon={faCheck} />
      ) : (
        <FontAwesomeIcon icon={faRss} />
      )}
      <button className={styles.button} onClick={handleCopyClick}>
        {isCopied ? "Copied!" : label}
      </button>
    </div>
  );
}
