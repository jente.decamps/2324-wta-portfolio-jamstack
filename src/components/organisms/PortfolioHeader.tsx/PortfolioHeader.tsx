"use client";

import { useState } from "react";
import { usePathname } from "next/navigation";
import styles from "./PortfolioHeader.module.css";

export default function PortfolioHeader() {
  const pathname = usePathname();
  const [ariaExpanded, setAriaExpanded] = useState(false);
  return (
    <header>
      <div className={styles.content}>
        <a className={styles.logo} href="/">
          {process.env.NEXT_PUBLIC_USERNAME}&apos;s portfolio
        </a>
        <button
          aria-expanded={ariaExpanded}
          className={styles.hamburger}
          id={styles.menu}
          onClick={() => setAriaExpanded(!ariaExpanded)}
        >
          <span aria-hidden="true" className={styles.icon}>
            <span></span>
            <span></span>
            <span></span>
          </span>
          <span>Menu</span>
        </button>
        <nav>
          <ul>
            <li>
              <a className={pathname === "/" ? styles.active : ""} href="/">
                Home
              </a>
            </li>
            <li>
              <a
                className={
                  pathname.startsWith("/projects") ? styles.active : ""
                }
                href="/projects"
              >
                Projects
              </a>
            </li>
            <li>
              <a
                className={pathname.startsWith("/posts") ? styles.active : ""}
                href="/posts"
              >
                Posts
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
}
