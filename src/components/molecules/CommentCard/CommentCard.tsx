import DateFormat from "@/helpers/DateFormat";
import Comment from "@/interfaces/Comment";
import styles from "./CommentCard.module.css";

export default function CommentCard({ comment }: { comment: Comment }) {
  return (
    <div className={styles.container}>
      <h3>{comment.author}</h3>
      <p>{comment.message}</p>
      <p className={styles.createdAt}>{DateFormat(new Date(comment.createdAt))}</p>
    </div>
  );
}
