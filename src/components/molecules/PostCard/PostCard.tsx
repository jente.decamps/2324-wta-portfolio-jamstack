import styles from "./PostCard.module.css";
import Post from "@/interfaces/Post";
import DateFormat from "@/helpers/DateFormat";

export default function PostCard({ post }: { post: Post }) {
  const truncateText = (text: string, maxLength: number) => {
    return text.length > maxLength
      ? text.substring(0, maxLength) + "..."
      : text;
  };

  return (
    <article className={styles.container}>
      <h2>{post.title}</h2>
      <p>{truncateText(post.message, 150)}</p>
      <p className={styles.createdAt}>{DateFormat(new Date(post.createdAt))}</p>
      <a href={`/posts/${post.id}`}>Read this post</a>
    </article>
  );
}
