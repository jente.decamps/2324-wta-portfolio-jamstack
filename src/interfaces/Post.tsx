import Comment from "./Comment";

export default interface Post {
  id: number;
  title: string;
  message: string;
  createdAt: Date;
  comments: Comment[];
}
