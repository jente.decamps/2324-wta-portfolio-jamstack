export default async function fetchGraphQL(query: string) {
  const { data } = await fetch(`${process.env.NEXT_PUBLIC_GRAPHQL_URL}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ query }),
  })
    .then((res) => res.json())
    .catch((error) => console.error(error));
  return data;
}
