This is a [Next.js](https://nextjs.org/) JAMstack portfolio template bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

Make it your own with the following projects:

- [Portfolio_API](https://github.com/SennaUyttersprot03/2324-WTA-Portfolio_API)
- [Portfolio_CMS](https://github.com/SennaUyttersprot03/2324-WTA-Portfolio_CMS)

## Authors

- [Jente De Camps](https://gitlab.com/jente.decamps)
- [Senna Uyttersprot](https://gitlab.com/senna.uyttersprot)

## Getting Started

First, copy paste the [`.env.example`](.env.example) file to a `.env.local` file and fill in the required environment variables.

Then, install the dependencies:

```bash 
npm install
# or
yarn install
# or
pnpm install
# or
bun install
```

Finally, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

This portfilio can easily be provided with custom content via [this headless CMS](https://github.com/SennaUyttersprot03/2324-WTA-Portfolio_CMS).

## Deploy on Vercel

The easiest way to deploy your portfolio is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Don't forget to [set the environment variables in the Vercel dashboard](https://vercel.com/docs/projects/environment-variables). Otherwise, the portfolio will not work.

To finish the deployment, you need to set up a deploy hook in the [headless CMS](https://github.com/SennaUyttersprot03/2324-WTA-Portfolio_CMS) to trigger a rebuild of the portfolio on Vercel. You can [generate a deploy hook on Vercel](https://vercel.com/docs/deployments/deploy-hooks#creating-a-deploy-hook) in the settings of your project.

If you still have some questions, check out the [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
